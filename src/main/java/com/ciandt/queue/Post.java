package com.ciandt.queue;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Hashtable;

public class Post {

    private final static String JNDI_FACTORY="weblogic.jndi.WLInitialContextFactory";
    private final static String JMS_FACTORY="jms/ExtracaoExecucaoIntegracaoFactory";
    private final static String QUEUE_NAME="jms/ExtracaoExecucaoIntegracaoQueue";
    private final static String IP = "t3://10.253.195.31:8901";

    public static void main(String[] args) throws Exception {

        QueueConnectionFactory qconFactory;
        QueueConnection qcon;
        QueueSession qsession;
        QueueSender qsender;
        Queue queue;
        TextMessage message;

        String ip = IP;

        if (args != null && args.length > 0) {
            ip = args[0];
        }

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, ip);
        Context context = new InitialContext(env);

        qconFactory = (QueueConnectionFactory) context.lookup(JMS_FACTORY);
        qcon = qconFactory.createQueueConnection();
        qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        queue = (javax.jms.Queue) context.lookup(QUEUE_NAME);
        qsender = qsession.createSender(queue);
        message = qsession.createTextMessage();
        qcon.start();

        message.setText("Mensagem");
        qsender.send(message);

    }
}
